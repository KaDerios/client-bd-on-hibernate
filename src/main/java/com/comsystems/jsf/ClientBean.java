package com.comsystems.jsf;

import com.comsystems.service.ClientService;
import com.comsystems.tables.Client;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ManagedBean(name = "clientBean")
@RequestScoped
public class ClientBean {

	private Long id;
	private Long clientId;
	private String clientFirstname;
	private String clientSecondname;
	private String clientLastname;
	private String clientFullname;
	private String clientSex;
	private Date clientBirthdate;
	private String clientBirthplace;
	private Client client;
	private List<Client> clients;
	private final ClientService clientService = new ClientService();

	public String saveUser() {
		Client newClient = new Client();
		clientService.saveUser(updateClientInfo(newClient, "save"));
		return "index";
	}

	public String updateUser() {
		Client findingClient = clientService.findUser(id);
		clientService.updateUser(updateClientInfo(findingClient, "update"));
		return "index";
	}

	public String deleteUser() {
		Client client = clientService.findUser(id);
		if (client != null) {
			clientService.deleteUser(clientService.findUser(id));
			return "successDelete";
		}
		return "notFound";
	}

	public String findUser() {
		Client findingClient = clientService.findUser(id);
		if (findingClient != null) {
			setId(id);
			setClientFirstname(findingClient.getClientFirstname());
			setClientSecondname(findingClient.getClientSecondname());
			setClientLastname(findingClient.getClientLastname());
			setClientFullname(findingClient.getClientFullname());
			setClientBirthplace(findingClient.getClientBirthplace());
			return "";
		}
		return "notFound";
	}

	public void findAllUsers() {
		clients = new ArrayList<>();
		if (id != null) {
			clients.add(clientService.findUser(id));
		} else {
			clients.addAll(clientService.findAllUsers());
		}
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getClientFirstname() {
		return clientFirstname;
	}

	public void setClientFirstname(String clientFirstname) {
		this.clientFirstname = clientFirstname;
	}

	public String getClientSecondname() {
		return clientSecondname;
	}

	public void setClientSecondname(String clientSecondname) {
		this.clientSecondname = clientSecondname;
	}

	public String getClientLastname() {
		return clientLastname;
	}

	public void setClientLastname(String clientLastname) {
		this.clientLastname = clientLastname;
	}

	public String getClientFullname() {
		return clientFullname;
	}

	public void setClientFullname(String clientFullname) {
		this.clientFullname = clientFullname;
	}

	public String getClientSex() {
		return clientSex;
	}

	public void setClientSex(String clientSex) {
		this.clientSex = clientSex;
	}

	public Date getClientBirthdate() {
		return clientBirthdate;
	}

	public void setClientBirthdate(Date clientBirthdate) {
		this.clientBirthdate = clientBirthdate;
	}

	public String getClientBirthplace() {
		return clientBirthplace;
	}

	public void setClientBirthplace(String clientBirthplace) {
		this.clientBirthplace = clientBirthplace;
	}

	private Client updateClientInfo(Client client, String request) {
		client.setClientFirstname(clientFirstname);
		client.setClientSecondname(clientSecondname);
		client.setClientLastname(clientLastname);
		client.setClientFullname(getFullname());
		client.setClientSex(clientSex);
		client.setClientBirthplace(clientBirthplace);
		if ("save".equals(request)) {
			client.setClientBirthdate(new Date());
		}
		return client;
	}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	private String getFullname() {
		return clientFirstname + " "
			   + clientSecondname + " "
			   + clientLastname;
	}
}
