package com.comsystems.service;

import com.comsystems.dao.IUserDao;
import com.comsystems.dao.impl.ClientDao;
import com.comsystems.tables.Client;

import java.util.List;

public class ClientService {

	private final IUserDao clientsDao = new ClientDao();

	public ClientService() {
	}

	public Client findUser(Long id) {
		return clientsDao.findById(id);
	}

	public void saveUser(Client client) {
		clientsDao.save(client);
	}

	public void deleteUser(Client client) {
		clientsDao.delete(client);
	}

	public void updateUser(Client client) {
		clientsDao.update(client);
	}

	public List<Client> findAllUsers() {
		return clientsDao.findAll();
	}
}
