package com.comsystems.dao.impl;

import com.comsystems.dao.IUserDao;
import com.comsystems.factory.HibernateSessionFactoryUtil;
import com.comsystems.tables.Client;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ClientDao implements IUserDao {

	@Override
	public Client findById(Long id) {
		return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Client.class, id);
	}

	@Override
	public void save(Client client) {
		executeRequest("save", client);
	}

	@Override
	public void update(Client client) {
		executeRequest("update", client);
	}

	@Override
	public void delete(Client client) {
		executeRequest("delete", client);
	}

	@Override
	public List<Client> findAll() {
		return (List<Client>) HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From Client").list();
	}

	private void executeRequest(String request, Client client){
		Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
		Transaction tx1 = session.beginTransaction();
		if ("save".equals(request)){
			session.save(client);
		} else if ("update".equals(request)){
			session.update(client);
		} else if ("delete".equals(request)){
			session.delete(client);
		}
		tx1.commit();
		session.close();
	}
}