package com.comsystems.dao;

import com.comsystems.tables.Client;

import java.util.List;

public interface IUserDao {

	Client findById(Long id);

	void save(Client client);

	void update(Client client);

	void delete(Client client);

	List<Client> findAll();
}
