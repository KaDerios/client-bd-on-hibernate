package com.comsystems.tables;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "clients")
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "client_id")
	private Long clientId;
	@Column(name = "client_firstname")
	private String clientFirstname;
	@Column(name = "client_secondname")
	private String clientSecondname;
	@Column(name = "client_lastname")
	private String clientLastname;
	@Column(name = "client_fullname")
	private String clientFullname;
	@Column(name = "client_sex")
	private String clientSex;
	@Column(name = "client_birthdate")
	private Date clientBirthdate;
	@Column(name = "client_birthplace")
	private String clientBirthplace;

	public Client() {
	}

	public Client(Long clientId, String clientFirstname, String clientSecondname, String clientLastname, String clientFullname, String clientSex, Date clientBirthdate, String clientBirthplace) {
		this.clientId = clientId;
		this.clientFirstname = clientFirstname;
		this.clientSecondname = clientSecondname;
		this.clientLastname = clientLastname;
		this.clientFullname = clientFullname;
		this.clientSex = clientSex;
		this.clientBirthdate = clientBirthdate;
		this.clientBirthplace = clientBirthplace;
	}

	public Long getId() {
		return id;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getClientFirstname() {
		return clientFirstname;
	}

	public void setClientFirstname(String clientFirstname) {
		this.clientFirstname = clientFirstname;
	}

	public String getClientSecondname() {
		return clientSecondname;
	}

	public void setClientSecondname(String clientSecondname) {
		this.clientSecondname = clientSecondname;
	}

	public String getClientLastname() {
		return clientLastname;
	}

	public void setClientLastname(String clientLastname) {
		this.clientLastname = clientLastname;
	}

	public String getClientFullname() {
		return clientFullname;
	}

	public void setClientFullname(String clientFullname) {
		this.clientFullname = clientFullname;
	}

	public String getClientSex() {
		return clientSex;
	}

	public void setClientSex(String clientSex) {
		this.clientSex = clientSex;
	}

	public Date getClientBirthdate() {
		return clientBirthdate;
	}

	public void setClientBirthdate(Date clientBirthdate) {
		this.clientBirthdate = clientBirthdate;
	}

	public String getClientBirthplace() {
		return clientBirthplace;
	}

	public void setClientBirthplace(String clientBirthplace) {
		this.clientBirthplace = clientBirthplace;
	}
}
