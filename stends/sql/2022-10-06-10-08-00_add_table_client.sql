create table clients
(
	id                numeric(15) not null,
	client_id         numeric(15),
	client_firstname  varchar(50),
	client_secondname varchar(50),
	client_lastname   varchar(50),
	client_fullname   varchar(50),
	client_sex        varchar(50),
	client_birthdate  date,
	client_birthplace varchar(255),

	constraint clients_pk primary key (id)
);